path "sys/health"
{
  capabilities = ["read"]
}

path "devops/+/" {
  capabilities = ["list"]
}

path "devops/+/inventories/" {
  capabilities = ["list"]
}

path "devops/+/+/infra/*" {
  capabilities = ["create", "read", "update", "patch", "list"]
}

path "devops/data/inventories/infra/gitlab-runners" {
  capabilities = ["create", "read", "update", "patch", "list"]
}

# ns/product ns/projects
path "devops/+/+/ns/" {
  capabilities = ["list"]
}

path "devops/+/+/+/products/" {
  capabilities = ["list"]
}

path "devops/+/+/+/projects/" {
  capabilities = ["list"]
}

path "devops/+/+/+/products/*" {
  capabilities = ["create", "read", "update", "patch", "delete", "list"]
}

path "devops/+/+/+/projects/*" {
  capabilities = ["create", "read", "update", "patch", "delete", "list"]
}

# geonature/annotation
path "devops/+/+/geonature/" {
  capabilities = ["list"]
}

path "devops/+/+/+/annotation" {
  capabilities = ["create", "read", "update", "patch", "list"]
}

